(function ($) {
  // animations base
  function isInView(elem) {
    return (
      $(elem).offset().top - $(window).scrollTop() < $(elem).height() + 450
    );
  }

  $(document).ready(function () {
    $(".main-menu__trigger").click(function () {
      $(this).toggleClass("active");
      $("header#masthead .lower-header").toggleClass("active");
      $("header#masthead .main-menu").toggleClass("active");
      $("body").toggleClass("active-menu");
    });

    $(".wpcf7 .wpcf7-submit").wrap('<div class="input-wrapper"></div>');

    $(".single-product-wrapper__related .add_to_cart_button ").wrap('<div class="btn-cart-wrapper"></div>');

    $(".single-product-wrapper__reviews .form-submit input ").wrap('<div class="submit-wrapper"></div>');


    $(".home-categories__content p").addClass("animate-2");

    // animations
    $(window).on("scroll", function () {
      $(".animate").each(function () {
        if (isInView($(this))) {
          $(this).addClass("in-viewport-fade-in");
        }
      });
    });

    $(window).on("scroll", function () {
      $(".animate-2").each(function () {
        if (isInView($(this))) {
          $(this).addClass("in-viewport-full-opacity");
        }
      });
    });

    $(window).on("scroll", function () {
      $(".animate-3").each(function () {
        if (isInView($(this))) {
          $(this).addClass("in-viewport-opacity");
        }
      });
    });

    $(window).on("scroll", function () {
      $(".animate-4").each(function () {
        if (isInView($(this))) {
          $(this).addClass("in-viewport-rotate");
        }
      });
    });

    $("#search-overlay-trigger").on("click", function () {
      $(".search-overlay").addClass("active");
      $("body").addClass("overflow-hidden");
    });

    $("#close-search-overlay-trigger").on("click", function () {
      $(".search-overlay").removeClass("active");
      $("body").removeClass("overflow-hidden");
    });

    $(".panel-heading").on("click", function () {
      $(this).find(".product-collapse__toggle").toggleClass("active");
    });

    // Accordion in sidebar - shop page
    // Brands


    $(".widget .widget-title").each(function () {
      $(this).on("click", function () {
        $(this).parent().find('ul').toggleClass('active');
        $(this).toggleClass("active-h2");
      });
    });

    // Delete brackets from count attributes
    $(".woocommerce-widget-layered-nav-list .count").each(function () {
      $(this).html(/(\d+)/g.exec($(this).html())[0]);
    });
    // end Delete brackets from count attributes

    // Hide placeholder on billing details page
    $(".checkout-shop .woocommerce-input-wrapper").on("keyup", function () {
      $(this).parent().find('label').addClass(
        "hide-placeholder"
      );
    });

    $(".checkout-shop form input").on("change", function () {
      if( ! $(this).val() ) {
        $(this).parent().parent().find('label').removeClass(
            "hide-placeholder"
        );
      }
    });

    $(".checkout-shop .woocommerce-input-wrapper input").each(function () {
      if( $(this).val() ) {
        $(this).parent().parent().find('label').addClass(
            "hide-placeholder"
        );
      }
    });
    // end hide placeholder on billing details page

// reviews hide placeholder 

  });
})(jQuery);
