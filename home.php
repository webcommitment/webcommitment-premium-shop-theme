<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

get_header();

$blog = get_queried_object();
$blog_id = $blog->ID;

?>
<article id="page-blog" class="blog-content">
    <header class="entry-header page__title">
        <div class="container-fluid">
            <h1 class="entry-title "> <?php echo get_the_title( $blog_id); ?></h1>
            <div class="breadcrumbs">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </header><!-- .entry-header -->
    <?php get_template_part( 'template-parts/content', 'newsarchive' ); ?>
</article>
<?php
get_footer();