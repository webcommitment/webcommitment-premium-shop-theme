<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

$icon_page = get_field( 'icon'); 

?>

<article id="post-<?php the_ID(); ?>" class="post-content">
    <header class="entry-header">
        <div class="container-fluid row align-items-center">
            <!-- page icon -->
            <?php if(!empty ($icon_page)): ?>
            <div class="entry-header__icon">
                <img src="<?php echo $icon_page['url']; ?>" alt="" />
            </div>
            <?php endif; ?>
            <!-- end page icon -->
            <div class="entry-header__title">
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                <div class="breadcrumbs">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </header><!-- .entry-header -->

  
    <section class="entry-content my-account">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
</article>