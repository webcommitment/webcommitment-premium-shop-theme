<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

?>

<article class="" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header ">
        <div class="container-fluid">

            <?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title col-12">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title col-12"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
            <div class="entry-meta col-12">
                <?php webcommitment_starter_posted_on(); ?>
            </div><!-- .entry-meta -->
            <?php
		endif; ?>
            <div class="breadcrumbs">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </header><!-- .entry-header -->

    <section class="entry-content">
        <div class="container-fluid">
            <div class="blog-content__single">
                <div class="col-12 col-sm-12 col-md-8">
                    <div class="blog-content__single-text">
                        <?php
                        the_content( sprintf(
                            wp_kses(
                                /* translators: %s: Name of current post. */
                                __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'webcommitment-theme' ),
                                array(
                                    'span' => array(
                                        'class' => array(),
                                    ),
                                )
                            ),
                            the_title( '<span class="screen-reader-text">"', '"</span>', false )
                        ) );

                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'webcommitment-theme' ),
                            'after'  => '</div>',
                        ) );
                        ?>
                    </div>
                </div>
                <div class="col-10 col-sm-10 col-md-6 col-lg-4 blog-content__single-img">
                    <img src="<?php echo the_post_thumbnail_url(); ?>"></img>
                </div>
            </div>
        </div>
        </div>
    </section><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->